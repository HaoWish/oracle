## 实验1：SQL语句的执行计划分析与优化指导

姓名：彭浩

学号：202010414115

### 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

### 实验数据库和用户

数据库是pdborcl，用户是sys和hr

### 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。



### 实验过程

#### 1.向用户hr授予视图选择权限：

连接数据库：

```
sqlplus sys/123@localhost/pdborcl as sysdba
```

进行权限分配：

```
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

结果：

```
SQL> create role plustrace;
角色已创建。 
SQL> grant select on v_$sesstat to plustrace;
授权成功。
SQL> grant select on v_$statname to plustrace;
授权成功。
SQL> grant select on v_$mystat to plustrace;
授权成功。
SQL> grant plustrace to dba with admin option;
授权成功。
SQL> set echo off
授权成功。
授权成功。
授权成功。
授权成功。
授权成功。
```

#### 2.进行查询

##### 1）查询1

查询所有部门的部门总人数和最高工资：

```
// 连接数据库
sqlplus hr/123@localhost/pdborcl
// 启用 STATISTICS 报告
set autotrace on
// Sql语句
SELECT d.department_name,count(e.job_id)as "部门总人数",
max(e.salary)as "最高工资"
from hr.departments d,hr.employees e
where d.department_id = e.department_id
GROUP BY d.department_name;

// 结果 
DEPARTMENT_NAME                部门总人数   最高工资
------------------------------ ---------- ----------
Administration				1	4400
Accounting				2      12008
Purchasing				6      11000
Human Resources 			1	6500
IT					5	9000
Public Relations			1      10000
Executive				3      24000
Shipping			       45	8200
Sales				       34      14000
Finance 				6      12008
Marketing				2      13000

执行计划
----------------------------------------------------------
Plan hash value: 1139150879

--------------------------------------------------------------------------------
-------------

| Id  | Operation		      | Name	    | Rows  | Bytes | Cost (%CPU
)| Time     |

--------------------------------------------------------------------------------
-------------

|   0 | SELECT STATEMENT	      | 	    |	 27 |	621 |	  7  (29
)| 00:00:01 |

|   1 |  HASH GROUP BY		      | 	    |	 27 |	621 |	  7  (29
)| 00:00:01 |

|   2 |   MERGE JOIN		      | 	    |	106 |  2438 |	  6  (17
)| 00:00:01 |

|   3 |    TABLE ACCESS BY INDEX ROWID| DEPARTMENTS |	 27 |	432 |	  2   (0
)| 00:00:01 |

|   4 |     INDEX FULL SCAN	      | DEPT_ID_PK  |	 27 |	    |	  1   (0
)| 00:00:01 |

|*  5 |    SORT JOIN		      | 	    |	107 |	749 |	  4  (25
)| 00:00:01 |

|   6 |     TABLE ACCESS FULL	      | EMPLOYEES   |	107 |	749 |	  3   (0
)| 00:00:01 |

--------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

   5 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")
       filter("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")


```

##### 2）查询2

对查询1进行优化指导：

运行sql优化指导：

![image-20230410222954322](实验1：SQL语句的执行计划分析与优化指导.assets/image-20230410222954322.png)

似乎没有指导：

![image-20230410223036184](实验1：SQL语句的执行计划分析与优化指导.assets/image-20230410223036184.png)

优化结束。



### 实验结果与分析

成功完成了实验一的sql语句的设计与查询。

似乎在使用一些比较复杂的sql语句时才会有优化指导。