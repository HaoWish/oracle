# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 实验过程

##### (1)建立杨辉三角

```sql
create or replace PROCEDURE    YHTriangle(N IN NUMBER) AS TYPE 
t_number IS VARRAY(100) OF INTEGER NOT NULL; 
j INTEGER; spaces VARCHAR2(30) :=' ';  
rowArray t_number := t_number(); 
BEGIN 
DBMS_OUTPUT.PUT_LINE('1'); 
DBMS_OUTPUT.PUT(RPAD(1,9,' '));
DBMS_OUTPUT.PUT(RPAD(1,9,' ')); 
DBMS_OUTPUT.PUT_LINE(''); 
FOR i IN 1 .. N LOOP 
rowArray.EXTEND; 
END LOOP; 
rowArray(1):=1; 
rowArray(2):=1; 
FOR i IN 3 .. N LOOP - 
rowArray(i):=1; 
j:=i-1;
WHILE j>1 LOOP  
rowArray(j):=rowArray(j)+rowArray(j-1); 
j:=j-1; 
END LOOP; 
FOR j IN 1 .. i LOOP 
DBMS_OUTPUT.PUT(RPAD(rowArray(j),9,' '));
END LOOP; DBMS_OUTPUT.PUT_LINE(''); 
END LOOP; END YHTriangle;
```

##### (2)激活输出

```sql
SET SERVEROUTPUT ON SIZE UNLIMITED 
```

##### (3)执行存储过程

```sql
EXECUTE hr.YHTriangle(5); 
```

## 实验结果

![image-20230523175650826](实验4：PLSQL语言打印杨辉三角.assets/image-20230523175650826.png)