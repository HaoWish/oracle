# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

****

**学号**：202010414115

**姓名**：彭浩

****

登录数据库：

```
sqlplus system/123@dsms
```

![image-20230525202118495](test6.assets/image-20230525202118495.png)

## 一、表空间设计

1. 表空间1：SYSTEM 表空间（用于存储系统表和索引）
2. 表空间2：USERS 表空间（用于存储用户数据）

```
-- 创建表空间
CREATE TABLESPACE system_ts
  DATAFILE '/home/oracle/Desktop/Oracle/system_ts.dbf'
  SIZE 100M
  AUTOEXTEND ON
  NEXT 10M
  MAXSIZE UNLIMITED
  LOGGING;

CREATE TABLESPACE users_ts
  DATAFILE '/home/oracle/Desktop/Oracle/users_ts.dbf'
  SIZE 200M
  AUTOEXTEND ON
  NEXT 50M
  MAXSIZE UNLIMITED
  LOGGING;

```

```
执行结果：表空间已创建。
```

![image-20230525202140545](test6.assets/image-20230525202140545.png)



## 二、表设计

1. 商品表（Products）：

   - 列：商品ID（ProductID，主键），商品名称（ProductName），商品描述（ProductDescription），商品价格（Price），商品库存（Stock）等。

   ```
   -- 创建商品表
   CREATE TABLE products (
     product_id NUMBER PRIMARY KEY,
     product_name VARCHAR2(100),
     product_description VARCHAR2(200),
     price NUMBER,
     stock NUMBER
   )
   TABLESPACE users_ts;
   
   ```

   ```
   表已创建。
   ```

   ![image-20230525202159246](test6.assets/image-20230525202159246.png)

2. 客户表（Customers）：

   - 列：客户ID（CustomerID，主键），客户姓名（CustomerName），客户地址（Address），联系电话（PhoneNumber）等。

   ```
   
   -- 创建客户表
   CREATE TABLE customers (
     customer_id NUMBER PRIMARY KEY,
     customer_name VARCHAR2(100),
     address VARCHAR2(200),
     phone_number VARCHAR2(20)
   )
   TABLESPACE users_ts;
   
   ```

   ```
   表已创建。
   ```

   ![image-20230525202211168](test6.assets/image-20230525202211168.png)

3. 订单表（Orders）：

   - 列：订单ID（OrderID，主键），客户ID（CustomerID，外键），订单日期（OrderDate），订单总额（TotalAmount）等。

   ```
   
   -- 创建订单表
   CREATE TABLE orders (
     order_id NUMBER PRIMARY KEY,
     customer_id NUMBER,
     order_date DATE,
     total_amount NUMBER,
     CONSTRAINT fk_customer
       FOREIGN KEY (customer_id)
       REFERENCES customers(customer_id)
   )
   TABLESPACE users_ts;
   
   ```

   ```
   表已创建。
   ```

   ![image-20230525202221911](test6.assets/image-20230525202221911.png)

4. 订单明细表（OrderDetails）：

   - 列：订单明细ID（DetailID，主键），订单ID（OrderID，外键），商品ID（ProductID，外键），商品数量（Quantity），商品单价（UnitPrice），小计金额（Subtotal）等。

   ```
   -- 创建订单明细表
   CREATE TABLE order_details (
     detail_id NUMBER PRIMARY KEY,
     order_id NUMBER,
     product_id NUMBER,
     quantity NUMBER,
     unit_price NUMBER,
     subtotal NUMBER,
     CONSTRAINT fk_order
       FOREIGN KEY (order_id)
       REFERENCES orders(order_id),
     CONSTRAINT fk_product
       FOREIGN KEY (product_id)
       REFERENCES products(product_id)
   )
   TABLESPACE users_ts;
   ```

   ```
   表已创建。
   ```

![image-20230525202232113](test6.assets/image-20230525202232113.png)

## 三、模拟数据生成

为满足要求，为每个表生成大约2.5万条记录，以达到总的模拟数据量不少于10万条的目标。

1.脚本实现

保存下面的代码到/home/oracle/desktop/Oracle/mock.sql

![image-20230525202313742](test6.assets/image-20230525202313742.png)

```
-- 生成商品表模拟数据
DECLARE
  i NUMBER := 1;
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO products (product_id, product_name, product_description, price, stock)
    VALUES (i, 'Product ' || i, 'Description for Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(10, 100), 0));
  END LOOP;
  COMMIT;
END;
/

-- 生成客户表模拟数据
DECLARE
  i NUMBER := 1;
BEGIN
  FOR i IN 1..25000 LOOP
    INSERT INTO customers (customer_id, customer_name, address, phone_number)
    VALUES (i, 'Customer ' || i, 'Address for Customer ' || i, '123-456-7890');
  END LOOP;
  COMMIT;
END;
/

-- 生成订单表模拟数据
DECLARE
  i NUMBER := 1;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orders (order_id, customer_id, order_date, total_amount)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 25000), 0), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365), 0), ROUND(DBMS_RANDOM.VALUE(10, 500), 2));
  END LOOP;
  COMMIT;
END;
/

-- 生成订单明细表模拟数据
DECLARE
  i NUMBER := 1;
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO order_details (detail_id, order_id, product_id, quantity, unit_price, subtotal)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 50000), 0), ROUND(DBMS_RANDOM.VALUE(1, 25000), 0), ROUND(DBMS_RANDOM.VALUE(1, 10), 0), ROUND(DBMS_RANDOM.VALUE(1, 100), 2), 0);
  END LOOP;
  COMMIT;
END;
/

```

![image-20230525202324813](test6.assets/image-20230525202324813.png)

2.执行脚本

在终端中执行：

```
sqlplus system/123@dsms @mock.sql
```

执行结果：

![image-20230525202401713](test6.assets/image-20230525202401713.png)

![image-20230525202420155](test6.assets/image-20230525202420155.png)



```
PL/SQL 过程已成功完成。
```

3.查看是否生成了数据

在SQL执行：

```
select count(*) from products;  -- 检查产品表行数
select count(*) from customers; -- 检查客户表行数
select count(*) from orders;    -- 检查订单表行数 
select count(*) from order_details; -- 检查订单明细表行数
```

执行结果：

```
SQL> select count(*) from products;
  COUNT(*)
----------
     25000
SQL> select count(*) from customers;
  COUNT(*)
----------
     25000
SQL> select count(*) from orders;
  COUNT(*)
----------
     50000
SQL> select count(*) from order_details;
  COUNT(*)
----------
    100000
```

![image-20230525195439304](test6.assets/image-20230525195439304.png)



## 四、用户及权限设计

基于系统需求，创建了两个角色：`sales_role`（销售角色）和`admin_role`（管理员角色）。

销售角色具有对商品表、订单表和订单明细表的查询和更新权限，用于销售人员进行商品信息和订单管理。

管理员角色具有对所有表的查询、插入、更新和删除权限，用于系统管理员进行完整的数据管理。

然后创建了两个用户：`sales_user`（销售用户）和`admin_user`（管理员用户）。

销售用户被分配了销售角色，以便其具有销售相关的权限。

管理员用户被分配了管理员角色，以便其具有完整的数据管理权限。

1.创建角色及权限

```
-- 创建角色
CREATE ROLE C##sales_role;

-- 授予商品表的查询和更新权限给角色
GRANT SELECT, UPDATE ON products TO C##sales_role;

-- 授予订单表和订单明细表的查询和更新权限给角色
GRANT SELECT, UPDATE ON orders TO C##sales_role;
GRANT SELECT, UPDATE ON order_details TO C##sales_role;

-- 创建角色
CREATE ROLE C##admin_role;

-- 授予所有表的查询和更新权限给管理员角色
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO C##admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO C##admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO C##admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO C##admin_role;

```

```
角色已创建。
授权成功。
授权成功。
授权成功。
角色已创建。
```

![image-20230525202659130](test6.assets/image-20230525202659130.png)

2.创建用户和分配角色

```
-- 创建销售用户
CREATE USER C##sales_user IDENTIFIED BY 123;

-- 分配销售角色给销售用户
GRANT C##sales_role TO C##sales_user;

-- 创建管理员用户
CREATE USER C##admin_user IDENTIFIED BY 123;

-- 分配管理员角色给管理员用户
GRANT C##admin_role TO C##admin_user;

```

![image-20230525202837691](test6.assets/image-20230525202837691.png)



## 五、业务逻辑设计及程序包实现

1.设计

1. 存储过程 `calculate_total_amount`：接收订单ID作为参数，计算订单的总金额，并更新订单表中的总金额字段。
2. 存储过程 `update_product_stock`：接收商品ID和数量作为参数，用于更新商品表中的库存数量。如果库存不足，可以根据实际需求处理，例如抛出异常或记录日志。
3. 函数 `get_customer_last_order_date`：接收客户ID作为参数，返回该客户的最近订单日期。

2.实现

将下面的代码保存到sale.sql：

![image-20230525203245484](test6.assets/image-20230525203245484.png)

![image-20230525203301524](test6.assets/image-20230525203301524.png)

```
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS

  -- 存储过程：计算订单总额
  PROCEDURE calculate_total_amount(p_order_id IN NUMBER);

  -- 存储过程：更新商品库存
  PROCEDURE update_product_stock(p_product_id IN NUMBER, p_quantity IN NUMBER);

  -- 函数：获取客户最近订单日期
  FUNCTION get_customer_last_order_date(p_customer_id IN NUMBER) RETURN DATE;

END sales_pkg;
/

-- 实现存储过程和函数
CREATE OR REPLACE PACKAGE BODY sales_pkg AS

  -- 存储过程：计算订单总额
  PROCEDURE calculate_total_amount(p_order_id IN NUMBER) IS
    v_total_amount NUMBER;
  BEGIN
    SELECT SUM(quantity * unit_price) INTO v_total_amount
    FROM order_details
    WHERE order_id = p_order_id;

    UPDATE orders
    SET total_amount = v_total_amount
    WHERE order_id = p_order_id;
  END calculate_total_amount;

  -- 存储过程：更新商品库存
  PROCEDURE update_product_stock(p_product_id IN NUMBER, p_quantity IN NUMBER) IS
    v_current_stock NUMBER;
  BEGIN
    SELECT stock INTO v_current_stock
    FROM products
    WHERE product_id = p_product_id;

    IF v_current_stock >= p_quantity THEN
      UPDATE products
      SET stock = stock - p_quantity
      WHERE product_id = p_product_id;
    ELSE
      -- 处理库存不足的情况，例如抛出异常或记录日志
      NULL;
    END IF;
  END update_product_stock;

  -- 函数：获取客户最近订单日期
  FUNCTION get_customer_last_order_date(p_customer_id IN NUMBER) RETURN DATE IS
    v_last_order_date DATE;
  BEGIN
    SELECT MAX(order_date) INTO v_last_order_date
    FROM orders
    WHERE customer_id = p_customer_id;

    RETURN v_last_order_date;
  END get_customer_last_order_date;

END sales_pkg;
/

```

3.使用脚本

1. 进入数据库。运行脚本

   ```
   sqlplus system/123@dsms
   SQL> @sale.sql
   ```

   ```
   程序包已创建。
   程序包体已创建。
   ```

   ![image-20230525203648779](test6.assets/image-20230525203648779.png)

2. 调用存储过程

   ```
   SQL> EXEC sales_pkg.calculate_total_amount(10);   -- 计算订单10的总额
   SQL> EXEC sales_pkg.update_product_stock(5, 10); -- 增加产品5的库存10
   
   ```

   ```
   PL/SQL 过程已成功完成。
   ```

   ![image-20230525204020425](test6.assets/image-20230525204020425.png)

3. 执行函数获取返回值

   ```
   SQL> SELECT sales_pkg.get_customer_last_order_date(3) FROM DUAL;  
   -- 返回客户3的最新订单日期
   ```

   ```
   SALES_PKG.GET_CUSTO
   -------------------
   2022-08-02 20:23:44
   ```

   ![image-20230525204137577](test6.assets/image-20230525204137577.png)

这样就完成了一个存储和查询过程。



## 六、数据库备份方案

本系统使用的备份方案为备份脚本和定时自动备份。

可以通过调用备份脚本来主动备份数据库，也可由系统自动备份数据库。

1.备份脚本

新建backup_script.sh

```
#!/bin/bash

# 定义备份目录和文件名
backup_dir="/home/oracle/Desktop/Oracle/dsms/backup"
backup_file="sales_system_backup_$(date +%Y%m%d_%H%M%S).dmp"

# 执行完全备份
expdp system/123@dsms FULL=Y DIRECTORY=DATA_PUMP_DIR DUMPFILE=$backup_dir/$backup_file LOGFILE=$backup_dir/backup.log
```

![image-20230525205120541](test6.assets/image-20230525205120541.png)

执行结果：

![image-20230525205210683](test6.assets/image-20230525205210683.png)

2.定时任务 

实际为系统自动调用备份脚本来实现自动备份。

打开终端，执行`crontab -e`命令，然后在打开的编辑器中添加以下行：

```
# 每天凌晨2点执行完全备份
0 2 * * * /bin/bash /home/oracle/Desktop/Oracle/backup_script.sh
```

上述备份方案使用了Oracle的数据泵（Data Pump）工具（expdp）进行数据库备份，通过执行备份脚本实现完全备份操作。定时任务（crontab）设置每天凌晨2点自动执行备份脚本，以便按计划进行数据库备份。



3.可拓展的备份方案

在此方案的基础上，可以增加增量备份和差异备份,完善备份方案。

一个更为完善的备份方案，由于能力所限，无法实现：

```
1. 全备: 每周日凌晨执行一次
2. 增量级备份:每天晚上执行一次
3. 差异备份:每月第一天执行一次
4. 设置备份文件的保留策略,如:
  - 全备: 2个月
  - 增量备份: 1周
  - 差异备份: 3个月
5. 备份后使用RMAN进行校验,并记录在日志中
6. 不使用明文密码,使用口令文件存储口令
7. 使用system用户crontab设置定时备份任务
```

